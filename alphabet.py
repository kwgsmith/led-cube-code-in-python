import hardwarecode as Cube

#draw an A on the cube
def drawA(): 
	"Draws an a to a vertical plane of the cube"
	frame = [[False for j in range(0,12)] for i in range(0,12)]
	twoRow = [[False for j in range(0,12)] for i in range(0,12)]
	#row 2
	#left side
	frame[7][0] = True
	frame[8][0] = True
	frame[9][0] = True
	#front side
	frame[0][2] = True
	frame[0][3] = True 
	frame[0][4] = True
	#right side
	frame[2][12] = True
	frame[3][12] = True
	frame[4][12] = True
	#run second row
	Cube.run(frame, 2)
	
	#row 3,4,6,7,8
	#left side
	twoRow[10][0] = True
	twoRow[6][0] = True
	#front side
	twoRow[0][1] = True
	twoRow[0][5] = True
	#right side
	twoRow[1][12] = True
	twoRow[5][12] = True
	#row 3 and 4
	Cube.run(twoRow,3)
	Cube.run(twoRow,4)
	
	#row 5
	frame = [[False for j in range(0,12)] for i in range(0,12)]
	#left side
	frame[10][0] = True
	frame[9][0] = True
	frame[8][0] = True
	frame[7][0] = True
	frame[6][0] = True
	#front side
	frame[0][1] = True
	frame[0][2] = True
	frame[0][3] = True
	frame[0][4] = True
	frame[0][5] = True
	#right side
	frame[1][12] = True
	frame[2][12] = True
	frame[3][12] = True
	frame[4][12] = True
	frame[5][12] = True
	Cube.run(frame, 5)

	#run row 6,7,8
	Cube.run(twoRow, 6)
	Cube.run(twoRow, 7)
	Cube.run(twoRow, 8)

for i in range (0, 100):
	drawA()

