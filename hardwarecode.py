#Code for the hardware interface towards the cube
import RPi.GPIO as GPIO
import random
import time

#single plane test
def test(num): #test the functionality of a single plane
	frame = [[True for l in xrange(12)] for k in xrange(12)]

	#fill 3d array with True
	for i  in range(0,num):
		print i
		for j in range(0, 12):
			print j 
			for m in range(0,200):
				#run the frame to the cube
				run(frame, j)
		erase()


#processes a frame of the animation and sends it to the cube's hardware
def run(frame, row): 
	"Process a frame of the animation and send it to the cube"
	#for each row
	latch = 0
	plane = 0
	while plane < len(frame):
		#get two planes of column information
		planeOne = frame[plane]
		planeTwo = frame[plane + 1]
		#divide into the data for the bus lines
		latchTwoData = planeOne[8:] + planeTwo[8:]
		latchOneData = planeOne[:8]
		latchThreeData = planeTwo[:8]
		#output data and clock decoder for each
		outputData(latchOneData)
		clkCB(decToBin(latch))
		if latch == 15:
			outputData(latchTwoData)
			clkU16()
			outputData(latchThreeData)
			clkU17()
		else:
			outputData(latchOneData)
			clkCB(decToBin(latch + 1))
			outputData(latchOneData)
			clkCB(decToBin(latch + 2))
		#increment plane number by 2
		plane = plane + 2
		#increment latch number by 3
		latch = latch + 3
	clkPB(decToBin(row))
	#pause before next frame
	pause()

#clock latch 17
def clkU17():
	"Clock the last latch"
	setHigh(3)
	setLow(3)

#clock latch 16
def clkU16():
	"Clock the second to last latch"
	setHigh(5)
	setLow(5)

#clock plane board to next row
def clkPB(row):
	"Clock the plane board to the next row"
	#choose which row to set the plane board
	GPIO.output(22, row[0])
	GPIO.output(23, row[1])
	GPIO.output(24, row[2])
	GPIO.output(26, row[3])

	setHigh(7)
	setLow(7)

#clock latch on the column board
def clkCB(latch):
	"Clock the latch on the column board"
	#choose which row to set the column board
	GPIO.output(11, latch[0])
	GPIO.output(12, latch[1])
	GPIO.output(13, latch[2])
	GPIO.output(15, latch[3])
	
	setHigh(10)
	setLow(10)

#Output data on the bus lines
def outputData(data):
	"Output data onto the bus lines for the cube"
	GPIO.output(16, data[0])
	GPIO.output(18, data[1])
	GPIO.output(19, data[2])
	GPIO.output(21, data[3])
	GPIO.output(22, data[4])
	GPIO.output(23, data[5])
	GPIO.output(24, data[6])
	GPIO.output(26, data[7])

def setHigh(pinNum): #set the pin high
	"Set a given pin number to the high state"
	GPIO.output(pinNum, True)

def setLow(pinNum): #set the pin high
	"Set a given pin number to the high state"
	GPIO.output(pinNum, False)

def decToBin(num): #converts an integer to a list of boolean values
	"Converts an integer into a list of boolean values"
	binaryNum = [int(x) for x in bin(num)[2:]]
	final = []

	if len(binaryNum) < 4:
		for i in range(0, 4 - len(binaryNum)):
			final.append(False)
	
	for val in range(0,len(binaryNum)):
		if binaryNum[val] == 1:
			final.append(True)
		else:
			final.append(False)
	return final
#function to keep the lights on for a certain period of time
def pause():
	"Wait for 500 milliseconds"
	int = 0
	for i in range(0,522):
		int = i + i		

#Setup function
def setup(): #setup all the lines for output
	"Setup function for all of the output lines"
	GPIO.setmode(GPIO.BOARD)
	GPIO.setwarnings(False)

	GPIO.setup(3, GPIO.OUT)
	GPIO.setup(5, GPIO.OUT)
	GPIO.setup(7, GPIO.OUT)
	GPIO.setup(10, GPIO.OUT)
	GPIO.setup(11, GPIO.OUT)
	GPIO.setup(12, GPIO.OUT)
	GPIO.setup(13, GPIO.OUT)
	GPIO.setup(15, GPIO.OUT)
	GPIO.setup(16, GPIO.OUT)
	GPIO.setup(18, GPIO.OUT)
	GPIO.setup(19, GPIO.OUT)
	GPIO.setup(21, GPIO.OUT)
	GPIO.setup(22, GPIO.OUT)
	GPIO.setup(23, GPIO.OUT)
	GPIO.setup(24, GPIO.OUT)
	GPIO.setup(26, GPIO.OUT)

def erase(): #erase entire cube
	"Erase cube before starting again"
	frame = [[False for l in xrange(12)] for k in xrange(12)]

	for i in range(0,12):
		run(frame, i)

	
#main body of code

#then = time.clock()

#for i in range(0, 20000):
#	pause()

#now = time.clock()

#print now - then
	
