import hardwarecode as Cube
import random as rand

#make it rain!
def rain(num):
	#make sure the cube is setup
	randRow = [[False for k in range(0,12)] for j in range(0,12)]
	
	#run for a certain number of iterations
	for i in range(0,num):
		#create a random number
		rNum = rand.randrange(20)
		#Loop for rNum times
		for j in range(0, rNum):
			#generate random True value in top row
			randRow[rand.randint(0,11)][rand.randint(0,11)] = True
		#copy row all the way through
		for k in range(0, 11):
			for l in range(0,200):
				Cube.run(randRow, k)
		Cube.erase()
	
#main code			
Cube.setup()

rain(5)

